package com.example.amol.testdemo.custom_font;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.example.amol.testdemo.utils.Constants;


/**
 * Created by Mankesh17 on 31-Jul-17.
 */
@SuppressWarnings("ALL")
public class TextViewBold extends android.support.v7.widget.AppCompatTextView {

    private Context mContext;

    public TextViewBold(Context context) {
        super(context);
        mContext = context;
        init();
    }

    public TextViewBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        init();
    }

    public TextViewBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }


    public void init(){
        Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), Constants.FONT_DIR+ Constants.FONT_BOLD);
        setTypeface(typeface);
    }
}
