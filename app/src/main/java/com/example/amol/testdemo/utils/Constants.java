package com.example.amol.testdemo.utils;

public class Constants {


    /***
     * Font constant
     */
    public static final String FONT_DIR = "font/";
    public static final String FONT_BOLD = "Raleway-Bold.ttf";
    public static final String FONT_REGULAR = "Raleway-Regular.ttf";
}
