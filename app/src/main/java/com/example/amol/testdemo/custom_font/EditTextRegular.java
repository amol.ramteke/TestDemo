package com.example.amol.testdemo.custom_font;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.example.amol.testdemo.utils.Constants;


/**
 * Created by Mankesh17 on 31-Jul-17.
 */
@SuppressWarnings("ALL")
public class EditTextRegular extends android.support.v7.widget.AppCompatEditText {

    private Context mContext;

    public EditTextRegular(Context context) {
        super(context);
        mContext = context;
        init();
    }

    public EditTextRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        init();
    }

    public EditTextRegular(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }


    public void init(){
        Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), Constants.FONT_DIR+ Constants.FONT_REGULAR);
        setTypeface(typeface);
    }
}
