package com.example.amol.testdemo.interfaces;

/**
 * Created by Amol_dev on 08-06-2018.
 */

public interface AuthenticateCallback {
    void authentication(String msg, boolean isAuthentic);
}
