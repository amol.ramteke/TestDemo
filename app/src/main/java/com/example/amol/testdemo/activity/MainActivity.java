package com.example.amol.testdemo.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;

import com.example.amol.testdemo.R;
import com.example.amol.testdemo.custom_font.ButtonBold;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.btn_english)
    ButtonBold btnEnglish;
    @BindView(R.id.btn_arebic)
    ButtonBold btnArebic;
    private Context mContext = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


    }

    public  void setApplicationlanguage(String language) {

        Resources res = mContext.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.setLocale(new Locale(language)); // API 17+ only.

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
//            conf.setLocale(new Locale(language)); // API 17+ only.
//        } else {
//            conf.locale = new Locale(language);
//        }
        res.updateConfiguration(conf, dm);
    }

    @OnClick({R.id.btn_english, R.id.btn_arebic})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_english:
                setApplicationlanguage("en");
                startActivity(new Intent(mContext,NavigationDrawerActivity.class));

                break;
            case R.id.btn_arebic:
                setApplicationlanguage("ar");
                startActivity(new Intent(mContext,NavigationDrawerActivity.class));
                break;
        }
    }


    @Override
    public void onBackPressed() {
        finish();
    }
}
