package com.example.amol.testdemo.handler;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.CancellationSignal;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.TextView;

import com.example.amol.testdemo.R;
import com.example.amol.testdemo.interfaces.AuthenticateCallback;

public class FingerprintHandler extends FingerprintManager.AuthenticationCallback {

    private AuthenticateCallback authenticateCallback;
    private Context context;


    // Constructor
    public FingerprintHandler(AuthenticateCallback mAuthenticateCallback) {

        this.authenticateCallback = mAuthenticateCallback;
    }


    public void startAuth(FingerprintManager manager, FingerprintManager.CryptoObject cryptoObject) {
        CancellationSignal cancellationSignal = new CancellationSignal();
        manager.authenticate(cryptoObject, cancellationSignal, 0, this, null);
    }


    @Override
    public void onAuthenticationError(int errMsgId, CharSequence errString) {
        this.update("Fingerprint Authentication error\n" + errString, false);
    }


    @Override
    public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
        this.update("Fingerprint Authentication help\n" + helpString, false);
    }


    @Override
    public void onAuthenticationFailed() {
        this.update("Fingerprint Authentication failed.", false);
    }


    @Override
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
        this.update("Fingerprint Authentication succeeded.", true);
    }


    public void update(String e, Boolean success){
       // TextView textView = (TextView) ((Activity)context).findViewById(R.id.errorText);
       // textView.setText(e);
        if(success){
         //   textView.setTextColor(ContextCompat.getColor(context,R.color.colorPrimaryDark));
            authenticateCallback.authentication("login successful",true);

        }
    }
}
